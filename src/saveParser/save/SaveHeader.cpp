//
// Created by piotr on 22.06.22.
//
#include "SaveHeader.h"

#include <utility>
#include <iostream>
#include <stdio.h>

#include "util/streamParse.h"

namespace parser::save {
    SaveHeader::SaveHeader(std::istream &stream) {
        headerVersion = util::getNumber<HeaderVersion>(stream);
        saveVersion = util::getNumber<SaveVersion>(stream);
        buildVersion = util::getNumber<std::int32_t>(stream);
//        mapName = util::getString(stream);
    }

    SaveHeader::SaveHeader(parser::save::HeaderVersion headerVersion,
                           parser::save::SaveVersion saveVersion,
                           int32_t buildVersion, std::string mapName,
                           std::string mapOptions,
                           std::string sessionName, int32_t playedSeconds, int64_t saveTimestamp,
                           parser::save::SessionVisibility sessionVisibility, int32_t editorObjectVersion,
                           std::string modMetadata, int32_t modFlags) : headerVersion(headerVersion),
                                                                               saveVersion(saveVersion),
                                                                               buildVersion(buildVersion),
                                                                               mapName(std::move(mapName)),
                                                                               mapOptions(std::move(mapOptions)),
                                                                               sessionName(std::move(sessionName)),
                                                                               playedSeconds(playedSeconds),
                                                                               saveTimestamp(saveTimestamp),
                                                                               sessionVisibility(sessionVisibility),
                                                                               editorObjectVersion(editorObjectVersion),
                                                                               modMetadata(std::move(modMetadata)),
                                                                               modFlags(modFlags) {}

}