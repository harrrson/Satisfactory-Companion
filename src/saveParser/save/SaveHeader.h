//
// Created by piotr on 21.06.22.
//

#pragma once

#include <cstdint>
#include <istream>

#include "./HeaderVersion.h"
#include "./SaveVersion.h"
#include "./SessionVisibility.h"

namespace parser::save {
    class SaveHeader {
    public:

        HeaderVersion headerVersion{};
        SaveVersion saveVersion{};
        std::int32_t buildVersion{};
        std::string mapName{};
        std::string mapOptions{};
        std::string sessionName{};
        std::int32_t playedSeconds{};
        std::int64_t saveTimestamp{};
        SessionVisibility sessionVisibility{};
        std::int32_t editorObjectVersion{};
        std::string modMetadata{};
        std::int32_t modFlags{};

        SaveHeader(std::istream&);

        SaveHeader(HeaderVersion headerVersion, SaveVersion saveVersion, int32_t buildVersion,
                   std::string mapName, std::string mapOptions, std::string sessionName,
                   int32_t playedSeconds, int64_t saveTimestamp, SessionVisibility sessionVisibility,
                   int32_t editorObjectVersion, std::string modMetadata, int32_t modFlags);
    };
}