//
// Created by piotr on 21.06.22.
//

#pragma once

#include <cstdint>

namespace parser::save{
    enum class SessionVisibility: std::int8_t{
        Private = 0,
        FriendsOnly = 1,
        Invalid
    };
}