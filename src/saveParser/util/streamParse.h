//
// Created by piotr on 22.06.22.
//

#pragma once
#include <cstdint>
#include <istream>

namespace parser::util {
    template<typename T>
    T getNumber(std::istream &data) {
        T n;
        data.read(reinterpret_cast<char *>(&n), sizeof(n));
        return n;
    }

    std::string getString(std::istream &);
}