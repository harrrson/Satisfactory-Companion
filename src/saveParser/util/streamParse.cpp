//
// Created by piotr on 22.06.22.
//

#include "streamParse.h"
#include <iostream>

namespace parser::util {
    std::string getString(std::istream & stream) {
        auto strLength = getNumber<std::int32_t>(stream);
        if(strLength == 0) return "";
        if(strLength<0) strLength *= -2;
        std::cout<<strLength<<'\n';
        char buffer[strLength];
        stream.read(buffer,strLength);
        return {buffer, size_t(strLength)};
    }
}