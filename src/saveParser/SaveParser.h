#pragma once

#include <istream>

namespace parser {
    class SaveParser {
    public:
        SaveParser(std::istream&);

        void parseFile();


    private:
        std::istream& stream;
    };
}