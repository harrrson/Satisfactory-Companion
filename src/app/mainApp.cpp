//
// Created by piotr on 17.06.2022.
//


#include <QApplication>
#include "windows/mainWindow/mainWindow.h"

int main(int argc, char **argv) {
    QApplication app(argc,argv);
    appUi::MainWindow window;
    window.show();
    return app.exec();
}
