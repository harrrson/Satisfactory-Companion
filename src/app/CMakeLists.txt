cmake_minimum_required(VERSION 3.16)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)
set(Qt6_DIR "/usr/lib/cmake/Qt6/")

set(HEADER_FILES
        windows/mainWindow/mainWindow.h)

set(SOURCE_FILES
        mainApp.cpp
        windows/mainWindow/mainWindow.cpp)

set(UI_FILES
        windows/mainWindow/mainWindow.ui
        )

add_executable(SatisfactoryCompanion ${SOURCE_FILES} ${HEADER_FILES} ${UI_FILES})

find_package(Qt6 REQUIRED COMPONENTS Core Widgets)

target_link_libraries(SatisfactoryCompanion Qt6::Core Qt6::Widgets)