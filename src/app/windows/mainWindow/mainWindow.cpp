//
// Created by piotr on 17.06.2022.
//


#include "mainWindow.h"
#include "ui_mainWindow.h"

namespace appUi {
    MainWindow::MainWindow(QWidget *parent) :
            QMainWindow(parent), ui(new Ui::MainWindow) {
        ui->setupUi(this);
    }

    MainWindow::~MainWindow() {
        delete ui;
    }
} // appUi
