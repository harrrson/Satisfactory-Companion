//
// Created by piotr on 21.06.22.
//
#include <gtest/gtest.h>
#include <sstream>
#include "save/SaveHeader.h"

TEST(SaveHeaderTest,AsciiOnlyStrings){
    std::stringstream asciiHeaderStream{};

    asciiHeaderStream
            << std::int8_t(0x09) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00)
            << std::int8_t(0x1C) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00)
            << std::int8_t(0xC1) << std::int8_t(0xE0) << std::int8_t(0x02) << std::int8_t(0x00)
            << std::int8_t(0X11) << "Persistent_Level"
            << std::int8_t(0x52) << "?startloc=Rocky Desert?sessionName=only one way to find out?Visibility=SV_Private"
            << std::int8_t(0x19) << "only one way to find out"
            << std::int8_t(0xCA) << std::int8_t(0x5D) << std::int8_t(0x0E) << std::int8_t(0x00)
            << std::int8_t(0xC0) << std::int8_t(0x53) << std::int8_t(0x62) << std::int8_t(0x91)
            << std::int8_t(0x00) << std::int8_t(0x42) << std::int8_t(0xDA) << std::int8_t(0x08)
            << std::int8_t(0x01)
            << std::int8_t(0x28) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00)
            << std::int8_t(0x10) << "TestModMetadata"
            << std::int8_t(0x02) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00);

    parser::save::SaveHeader expectedHeader{
            parser::save::HeaderVersion::UE426EngineUpdate,
            parser::save::SaveVersion::TrainBlueprintClassAdded,
            188609,
            "Persistent_Level",
            "?startloc=Rocky Desert?sessionName=only one way to find out?Visibility=SV_Private",
            "only one way to find out",
            941514,
            637894867432920000,
            parser::save::SessionVisibility::FriendsOnly,
            40,
            "TestModMetadata",
            2
    };
    parser::save::SaveHeader testedHeader{asciiHeaderStream};

    ASSERT_EQ(expectedHeader.headerVersion, testedHeader.headerVersion);
    ASSERT_EQ(expectedHeader.saveVersion, testedHeader.saveVersion);
    ASSERT_EQ(expectedHeader.buildVersion, testedHeader.buildVersion);
//    ASSERT_EQ(expectedHeader.mapName, testedHeader.mapName);
}