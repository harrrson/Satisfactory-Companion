//
// Created by piotr on 22.06.22.
//
#include <istream>
#include <cstdint>

#include <gtest/gtest.h>


#include "util/streamParse.h"

TEST(StreamParser, GetSingle32BitNumber) {
    std::stringstream data{};
    data << std::int8_t(0x14) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00);
    std::int32_t parsedNumber = parser::util::getNumber<std::int32_t>(data);
    ASSERT_EQ(20, parsedNumber);
}

TEST(StreamParser, GetMultiple32BitNumbers) {
    std::stringstream data{};
    data << std::int8_t(0x14) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00)
         << std::int8_t(0x20) << std::int8_t(0x23) << std::int8_t(0x10) << std::int8_t(0x57);
    std::int32_t firstNumber = parser::util::getNumber<std::int32_t>(data);
    std::int32_t secondNumber = parser::util::getNumber<std::int32_t>(data);

    ASSERT_EQ(20, firstNumber);
    ASSERT_EQ(1460675360, secondNumber);
}

TEST(StreamParser, GetSingle64BitNumber) {
    std::stringstream data{};
    data << std::int8_t(0x14) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00)
         << std::int8_t(0x22) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00);
    std::int64_t parsedNumber = parser::util::getNumber<std::int64_t>(data);
    ASSERT_EQ(146028888084, parsedNumber);
}
TEST(StreamParser, GetMixed32And64BitNumbers){
    std::stringstream data{};
    data << std::int8_t(0x20) << std::int8_t(0x23) << std::int8_t(0x10) << std::int8_t(0x57)
         << std::int8_t(0x14) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00)
         << std::int8_t(0x22) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00);
    std::int32_t firstNumber = parser::util::getNumber<std::int32_t>(data);
    std::int64_t secondNumber = parser::util::getNumber<std::int64_t>(data);

    ASSERT_EQ(1460675360, firstNumber);
    ASSERT_EQ(146028888084, secondNumber);
}

TEST(StreamParser, GetString){
    std::stringstream data{};
    data << std::int8_t(0x02) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00)
         << '.' << '\0';
    std::string parsedString = parser::util::getString(data);
    ASSERT_EQ(".", parsedString);
}

TEST(StreamParser, GetEmptyString){
    std::stringstream data{};
    data << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00) << std::int8_t(0x00);
    std::string parsedString = parser::util::getString(data);
    ASSERT_EQ("", parsedString);
}

TEST(streamParser, getUTF16String){
    std::stringstream data{};
    data << std::int8_t(0xFE) << std::int8_t(0xFF) << std::int8_t(0xFF) << std::int8_t(0xFF)
         << "Å" << '\0' << '\0';
    std::string parsedString = parser::util::getString(data);
    ASSERT_EQ("Å", parsedString);
}